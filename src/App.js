import React, { Component } from 'react';
import { BrowserRouter, Route, Switch} from 'react-router-dom';
 
import MainTemplate from "./components/mainLayout/template/mainTemplate";
import Home from "./components/home/home";
import Accedi from "./components/accedi/accedi";
import Profilo from "./components/profilo/profilo";
import Acquisti from "./components/acquisti/acquisti";
 
class App extends Component {
 
   constructor(props){
      super(props);
   }
 
   render() {
 
      return (
         <BrowserRouter>
            <MainTemplate>
               <Switch>
                   <Route exact path='/' component={Home}/>
                   <Route exact path='/acquisti' component={Acquisti}/> 
                   <Route exact path='/accedi' component={Accedi}/>
                   <Route exact path='/profilo' component={Profilo}/>
               </Switch>
            </MainTemplate>
         </BrowserRouter>
      );
   }
}
 //ciao
export default App;