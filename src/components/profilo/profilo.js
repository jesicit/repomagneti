import React from "react";
import QrReader from 'react-qr-reader'






export default class Profilo extends React.Component{
   constructor(props){
      super(props);
      this.registrato = this.registrato.bind(this);
      this.loadinfo = this.loadinfo.bind(this);
      this.entra = this.entra.bind(this);
      this.esci = this.esci.bind(this);
      this.logout = this.logout.bind(this);
   }



async entra(){
 
    const response = await fetch("https://magneticowork.altervista.org/webapp/funzioni/entrata.php?idutente="+window.localStorage.id);
    const data = await response.json();
console.log(data)

window.M.Toast.dismissAll();
if(data !== "errore"){
window.M.toast({html: data})
}
this.loadinfo();
await this.sleep(2500)
window.localStorage.setItem("dentro", "si")

}

async esci(){

    const response = await fetch("https://magneticowork.altervista.org/webapp/funzioni/uscita.php?idutente="+window.localStorage.id);
    const data = await response.json();
console.log(data)
if(data.sessione === "nessuna sessione corrente"){
   // window.M.Toast.dismissAll();
    //window.M.toast({html: "non puoi uscire senza entrare"})
}else{
    window.M.Toast.dismissAll();
    window.M.toast({html: data.paga+" "+data.tempo_da_pagare+" s"})
}
this.loadinfo(); 
await this.sleep(2500)
window.localStorage.setItem("dentro", "no")
}


logout(){
    this.esci()
    localStorage.clear();
    window.location.href = "/"
  }

   registrato(){
    if(!window.localStorage.username){
        window.location.href="accedi"
      }else{
        return true
      }
}


loadinfo(){
	

   
    let user = window.localStorage.username
    let pass = window.localStorage.pass
//ovunque
const l = async () => {
    const response = await fetch('https://magneticowork.altervista.org/webapp/funzioni/profilo.php?username='+user+'&pass='+pass);
    const data = await response.json();
    console.log(data)
    if(data.auth === "loggato"){
        document.querySelector("#pallino").style = "width: 12px;height: 12px;background: green;border-radius: 6px;"
    }else{
        document.querySelector("#pallino").style = "width: 12px;height: 12px;background: red;border-radius: 6px;"
        window.localStorage.setItem("pallino", "rosso")
    }
        document.querySelector("#tempo").innerText = "🕖 " +data.tempo
        document.querySelector("#nomeutente").innerText = data.username

        window.localStorage.setItem("id", data.id)
        window.localStorage.setItem("auth", data.auth)
        window.localStorage.setItem("tempo", data.tempo)

        window.localStorage.setItem("username", data.username)
        window.localStorage.setItem("pass", data.pass)
}
l();
}
sleep(ms){
    return new Promise(resolve => setTimeout(resolve, ms));
  }


state = {
    result: 'no'
  }

 
  handleScan = data => {
      
    if (data) {
        
        if(window.localStorage.dentro === "no"){
            this.entra()
           
        }else{
            this.esci()
            
        }
      this.setState({
        result: data
      })
     
    }
    
  }


  handleError = err => {
    console.error(err)
  }


   render(){
       this.loadinfo()
       this.registrato()
      return(
 
          <div>
    <p id="nomeutente"></p>
        <p>
        🤖 Web Entrepreneur
        </p>

        <div>
        <QrReader
          delay={300}
          onError={this.handleError}
          onScan={ this.handleScan }
          style={{ width: '100%' }}
        />
        <p>{this.state.result}</p>
        
      </div>


        <a onClick={() => { this.entra() }} class="waves-effect waves-light btn">entra</a>
        <a onClick={() => { this.esci() }} class="waves-effect waves-light btn">esci</a>
        <a onClick={() => { this.logout() }} class="waves-effect waves-light btn">logout</a>

</div>
      );
   }
}