import React from "react";
import './accedi.css';
import { Redirect } from 'react-router-dom'


export default class Accedi extends React.Component{ 
   constructor(props){ 
      super(props); 
      this.accedi = this.accedi.bind(this);
      this.registrato = this.registrato.bind(this);
      this.login = this.login.bind(this);
   }
   
   async accedi(){
    console.log("accedo")
      let user = document.querySelector("#username").value
      let pass = document.querySelector("#password").value
     this.login(user, pass)
      await this.sleep(2500)
      if(this.registrato()){
        window.location ="/profilo"
      }
  }
  

registrato(){
    if(!window.localStorage.username){
        
      }else{
        return <Redirect to='/target' />
      }
}

login(username, pass){
    const l = async () => {
        const response = await fetch('https://magneticowork.altervista.org/webapp/funzioni/login.php?username='+username+'&pass='+pass);
        const data = await response.json();
        console.log(data)
        if(data === "Logged"){
        window.localStorage.setItem("username", username)
        window.localStorage.setItem("pass", pass)
        }else{
          window.M.toast({html: "i dati inseriti sono errati"})
        }
    }
    l()
    }

    
sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

   render(){ 
     this.registrato()
      return(
          
          <div>

      <h5>Effettua il login 👋</h5>
      <div class="input-field col s12">
              <input id="username" type="text" class="white-text validate" />
              <label for="username">Username</label>
            </div>
    
    
            <div class="input-field col s12">
              <input id="password" type="password" class="white-text validate" />
              <label for="password">Password</label>
            </div>
       
    <a onClick={() => { this.accedi() }} class="waves-effect waves-light btn">accedi</a>     
    
    
    
    
    </div>



      ); 
   } 
}