import React from "react";
import { Link } from 'react-router-dom';
import './header.css';

export default class Header extends React.Component{ 
   constructor(props){ 
      super(props); 
   }
   render(){ 
      return( 
<nav class="color">
  <div class="nav-wrapper">
    <Link to="/" class="title brand-logo center"> Magneti Cowork </Link>
    <p id="tempo" class="right" ></p>
    <Link class="omino" to="profilo">👨‍🚀<i id="pallino"></i></Link>
    <ul id="nav-mobile" class="left hide">
      <li><Link to="sass.html">Login</Link></li>
      <li><Link to="badges.html">Chi siamo</Link></li>
      <li><Link to="collapsible.html">Contatti</Link></li>
    </ul>
  </div>
</nav>

      ); 
   }
}